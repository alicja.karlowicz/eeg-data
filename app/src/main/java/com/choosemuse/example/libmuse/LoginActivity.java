package com.choosemuse.example.libmuse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class LoginActivity extends Activity {

    private FirebaseAuth mAuth;

    private static final String TAG = "LoginActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity);
        mAuth = FirebaseAuth.getInstance();

    }



    public void onClick(View view) {


        EditText emailEditTxt = (EditText) findViewById(R.id.emailEditText);
        EditText passwordEditTxt = (EditText) findViewById(R.id.passwordEditText);

        final String email = emailEditTxt.getText().toString();
        String password = passwordEditTxt.getText().toString();

        if (!email.isEmpty() && !password.isEmpty()) {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                Log.d(TAG, "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                Toast.makeText(getApplicationContext(), "Authentication successfull",
                                        Toast.LENGTH_LONG).show();

                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                intent.putExtra("user", email);
                                LoginActivity.this.startActivity(intent);

                                //TODO: DODAJ DO MAINACTIVITY
                                // String user = getIntent().getStringExtra("user");

                            } else {

                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                Toast.makeText(getApplicationContext(), "Authentication failed",
                                        Toast.LENGTH_SHORT).show();

                            }

                                }
                            });
                } else
                    Toast.makeText(getApplicationContext(), "Fill in email and password",
                            Toast.LENGTH_SHORT).show();

    }
}
